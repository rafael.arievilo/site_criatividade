
// usei express para criar e configurar meu servidor
const express = require('express')
const server = express()

const db = require("./db")

// construção da variavel

/* const ideas = [
    {
        img: "https://image.flaticon.com/icons/svg/2729/2729007.svg",
        title: "Cursos de Programação",
        category: "Estudo",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi repellat, nobis officia dolor autem iste.",
        url: "https://rocketseat.com.br"
    },
    {
        img: "https://image.flaticon.com/icons/svg/2729/2729005.svg",
        title: "Exercicios",
        category: "Saúde",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi repellat, nobis officia dolor autem iste.",
        url: "https://rocketseat.com.br"
    },
    {
        img: "https://image.flaticon.com/icons/svg/2729/2729027.svg",
        title: "Meditação",
        category: "Mentalidade",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi repellat, nobis officia dolor autem iste.",
        url: "https://rocketseat.com.br"
    },
    {
        img: "https://image.flaticon.com/icons/svg/2729/2729021.svg",
        title: "Jogos",
        category: "Diversão",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi repellat, nobis officia dolor autem iste.",
        url: "https://rocketseat.com.br"
    }
] */

//configurar arquivos estaticos(css, scripts, imagens)

server.use(express.static("public"))

// habilitar uso do req.body
server.use(express.urlencoded({ extended: true }))


// configuração do nunjucks(var em html)

const nunjucks = require("nunjucks")
nunjucks.configure("views", {
    express: server,
    noCache: true,  // boolean
}) 

// criei uma rota /
// e capturo o pedido do cliente para responder
server.get("/", function(req, resp) {
    db.all(`SELECT * FROM ideas`, function(err, rows) {
               
        if (err) {
            console.log(err)
            return resp.send("Erro no banco de dados!")
        }

        const reverseIdeas = [...rows].reverse()

        let lastIdeas = []
        for (let idea of reverseIdeas) {
            if (lastIdeas.length < 2) {
                lastIdeas.push(idea)
            }
        }


        return resp.render("index.html", { ideas: lastIdeas })
        })

})


server.get("/ideias", function(req, resp) {

    

    db.all(`SELECT * FROM ideas`, function(err, rows) {

        if (err) {
            console.log(err)
            return resp.send("Erro no banco de dados!")
        }


        const reverseIdeas2 = [...rows].reverse()
        return resp.render("ideias.html", {ideas: reverseIdeas2})

    })


})

server.post("/", function(req, resp) {
    //Inserir dados na tabela
     const query = `
    INSERT INTO ideas(
        image,
        title,
        category,
        description,
        link
    ) VALUES (?, ?, ?, ?, ?);
    `

    const values = [
        req.body.image,
        req.body.title,
        req.body.category,
        req.body.description,
        req.body.link,
    ]



    db.run(query, values, function(err) {
        if (err) {
            console.log(err)
            return resp.send("Erro no banco de dados!")
        }

        return resp.redirect("/ideias")
     })

})

// liguei meu servidor na porta 30000
server.listen(3000)
